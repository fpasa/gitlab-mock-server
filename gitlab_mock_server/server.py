from sanic import Sanic
from sanic.response import text

server = Sanic("GitlabMockServer")


@server.get("/")
async def hello_world(request):
    return text("Hello, world.")
