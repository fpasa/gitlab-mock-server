from multiprocessing import Process


class GitlabMockServer:
    """Object representing the server, allowing users to terminate it.

    This might be helpful in test teardowns to shut down the server when
    not needed anymore.
    """

    def __init__(self, process: Process):
        self.process = process

    def terminate(self):
        self.process.kill()


def run_gitlab_mock(host: str = "localhost", port: int = 1080) -> GitlabMockServer:
    """Run a Gitlab mock server.

    This can be used to test code working against the Gitlab API without
    using a real Gitlab instance.

    This function runs the mock server on another process. The process
    is a daemon process, so it will be automatically be stopped when
    the calling process exits.

    :param host: Host where the mock server will be started.
    :param port: Port at which the mock server will run.
    :return: :class:`GitlabMockServer` instance to control the execution
        of the mock server.
    """
    server_proc = Process(target=_run_server, args=(host, port), daemon=True)
    server_proc.start()
    return GitlabMockServer(server_proc)


def _run_server(host: str, port: int):
    # Import here so that sanic does not get initialized
    # if the current process is just triggering the server
    # execution.
    from gitlab_mock_server.server import server

    server.run(host=host, port=port)
